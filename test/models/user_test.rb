require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @valid_user = User.new(name: "Nome Exemplo", email: "usuario@exemplo.com",
                           password: "senha1", password_confirmation: "senha1")
    @user = User.new(name: "outro Exemplo 2", email: "usuario2@exemplo.com",
                           password: "senha1", password_confirmation: "senha1")
  end

  test "A conceptually valid user should be valid" do
    assert @valid_user.valid?
  end

  test "The name must be present" do
    @user.name = " "
    assert_not @user.valid?
  end

  test "email must be present" do
    @user.email = " "
    assert_not @user.valid?
  end

  test "The name must not be too long" do
    @user.name = "a"*51
    assert_not @user.valid?
  end

  test "email must not be too long" do
    @user.email = "a"*244 + "@exemplo.com"
    assert_not @user.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email addresses should be unique" do
      duplicate_user = @user.dup
      duplicate_user.email = @user.email.upcase
      @user.save
      assert_not duplicate_user.valid?
    end

  test "email addresses should be saved as lower-case" do
   mixed_case_email = "Foo@ExAMPle.CoM"
   @user.email = mixed_case_email
   @user.save
   assert_equal mixed_case_email.downcase, @user.reload.email
 end

 test "password should be present (nonblank)" do
  @user.password = @user.password_confirmation = " " * 6
  assert_not @user.valid?
end

test "password should have a minimum length" do
  @user.password = @user.password_confirmation = "a" * 5
  assert_not @user.valid?
end



end
